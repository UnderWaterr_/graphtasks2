package com.company.graph2;

import java.util.HashMap;
import java.util.HashSet;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> vertex = new HashMap<>();
        vertex.put(startIndex, 0);
        for (int i = 0; i < adjacencyMatrix.length - 1; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (vertex.containsKey(j)) {
                    for (int k = 0; k < adjacencyMatrix.length; k++) {
                        if (adjacencyMatrix[j][k] != 0) {
                            if (vertex.containsKey(k)) {
                                if (adjacencyMatrix[j][k] + vertex.get(j) < vertex.get(k)) {
                                    vertex.put(k, adjacencyMatrix[j][k] + vertex.get(j));
                                }
                            } else {
                                vertex.put(k, adjacencyMatrix[j][k] + vertex.get(j));
                            }

                        }
                    }
                }
            }
        }
        return vertex;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        HashSet<Integer> vertex = new HashSet<>();
        vertex.add(0);
        int result = 0;
        for (int i = 0; i < adjacencyMatrix.length - 1; i++) {
            int minWayFromVertex = Integer.MAX_VALUE;
            int newVertex = 0;
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (vertex.contains(j)) {
                    for (int k = 0; k < adjacencyMatrix.length; k++) {
                        if (!vertex.contains(k) && adjacencyMatrix[j][k] != 0) {
                            if (minWayFromVertex > adjacencyMatrix[j][k]) {
                                minWayFromVertex = adjacencyMatrix[j][k];
                                newVertex = k;
                            }
                        }
                    }
                }
            }
            result += minWayFromVertex;
            vertex.add(newVertex);
        }
        return result;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        int lengthOfWay = 0;
        HashMap<Integer, Integer> pair = new HashMap<>();
        for (int i = 0; i < adjacencyMatrix.length; i++)
            pair.put(i, i);
        for (int i = 0; i < adjacencyMatrix.length - 1; i++) {
            int minWayNow = Integer.MAX_VALUE;
            int firstVertex = -1;
            int secondVertex = -1;
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                for (int k = 0; k < adjacencyMatrix.length; k++) {
                    if (findParentVertex(pair, j) != findParentVertex(pair, k) && adjacencyMatrix[j][k] < minWayNow && adjacencyMatrix[j][k] != 0) {
                        minWayNow = adjacencyMatrix[j][k];
                        firstVertex = j;
                        secondVertex = k;
                    }
                }
            }
            pair.put(findParentVertex(pair, firstVertex), findParentVertex(pair, secondVertex));
            lengthOfWay += minWayNow;
        }
        return lengthOfWay;
    }

    int findParentVertex(HashMap<Integer, Integer> pair, int i) {
        while (pair.get(i) != i)
            i = pair.get(i);
        return i;
    }
}
